from unittest import TestCase, main, mock
from random import choice
from TicTacToe2 import *
from unittest.mock import patch, MagicMock


class TestTicTacToe(TestCase):

    def setUp(self):
        self.game = TicTacToe()

    @patch('TicTacToe.curses.wrapper', return_value=0)
    def test_first_menu_you(self, mock_curses_1):
        value = first_menu()
        self.assertEqual(0, value)

    @patch('TicTacToe.robot_first', return_value=1)
    @patch('TicTacToe.curses.wrapper', return_value=1)
    def test_first_menu_robot(self, mock_curses_1, mock_robot_first):
        value = first_menu()
        self.assertEqual(1, value)

    @patch('TicTacToe.curses.wrapper', return_value=2)
    def test_first_menu_rock(self, mock_curses_1):
        value = first_menu()
        self.assertEqual(2, value)

    @patch('TicTacToe.exit', return_value="exit")
    @patch('TicTacToe.curses.wrapper', return_value=3)
    def test_first_menu_exit(self, mock_curses_1, mock_exit):
        value = first_menu()
        self.assertEqual("exit", value)


    @patch('TicTacToe.curses.wrapper', return_value=0)
    def test_rock_scissors_paper_menu_1(self, mock_curses_1):
        expected_value = "rock"
        value = rock_scissors_paper_menu()
        self.assertEqual(value, expected_value)

    @patch('TicTacToe.curses.wrapper', return_value=1)
    def test_rock_scissors_paper_menu_2(self, mock_curses_1):
        expected_value = "scissors"
        value = rock_scissors_paper_menu()
        self.assertEqual(value, expected_value)

    @patch('TicTacToe.curses.wrapper', return_value=2)
    def test_rock_scissors_paper_menu_3(self, mock_curses_1):
        expected_value = "paper"
        value = rock_scissors_paper_menu()
        self.assertEqual(value, expected_value)

    @patch('TicTacToe.curses.wrapper', return_value="done")
    @patch('TicTacToe.moves', return_value=[])
    # @patch('TicTacToe.human_check', return_value=True)
    # @patch('TicTacToe.computer_check', return_value=True)
    # @patch('TicTacToe.winner_test', return_value=True)
    def test_computer_move_len_0(self, mock_moves, mock_curses):
        expected_value = "done"
        value = computer_move()
        self.assertEqual(value, expected_value)



    # def test_winner_test(self):
    #     expected_value = "(_)"
    #     menu = ["(_)", "(_)", "(_)", "  ", "  ", "  ", "  ", "  ", "  "]
    #     win_positions = (
    #                     (0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
    #                     (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)
    #                     )
    #     value = winner_test()
    #     self.assertEqual(expected_value, value)



if __name__ == '__main__':
    main()
