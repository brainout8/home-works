#  Task 1

float_number = float(input("Type the float number here >>>> "))

print(
        f"Your number rounded to the integer = {round(float_number)}"
        f"\nRounded to the second decimal place = {round(float_number, 2)}"
        f"\nRounded to the fifth decimal place = {round(float_number, 5)}"
        )

#  Task 2

my_list = [3, 5, 'road', 'Кодзима', 90, 'last']

print(f"The last element of 'my_list' is: '{my_list[len(my_list)-1]}'")

#  Task 3

a = int(input("Type the first number(a) here >>>> "))
b = int(input("Type the second number(b) here >>>> "))
c = int(input("Type the third number(c) here >>>> "))

formula = input("Type your formula here >>>> ")

print(f"Result from using numbers by formula: {formula} = {eval(formula)}")

#  Task 4

my_list = ['hill', 111, 45, 'fast', 'silent', 90, 'last']

first_number = int(input("Type the first number from 0 to 6 here >>>> "))
second_number = int(input("Type the second number from 0 to 6 here >>>> "))

slice = my_list[first_number:second_number+1]

print(f"New list = {slice}")

# Task 5

user_string = input("Type your string here >>>> ")

print(
        f"The list of unique elements of your string = "
        f"{list(set(user_string))}"
        )

#  Task 6

first_set = {3, 4, 'bad', 23, 'real', 256, True}
second_set = {'fortress', 23, True, 'question', 'real', 256, False}

first_set.intersection_update(second_set)
difference = list(first_set)[0] - list(first_set)[-1]

print(f"Difference of the first and the last element = {difference}")

#  Task 7

first_list = [2, 4, 'do', 3, 'thinking', 546]
second_list = [3, 546, 'monkeys', 4, 'do', 'glory', 2]

third_list = set(first_list + second_list)

print(f"New list = {list(third_list)}")

#  Task 8

my_tuple = (2, 4, 56, 'Truth', 78, 2, 5)
my_list = [78, 5, 34, 7, 9, 'Truth']

first_set = set(my_tuple)
first_set.intersection_update(set(my_list))

print(f"List of mutual elements = {list(first_set)}")
