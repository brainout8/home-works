from re import findall, sub
from math import fsum
from my_methods import pathcheck, notampty, ifnumber, floatint
from datetime import date
from calendar import isleap


def text_check():
    """
    If file exists and not ampty,
    function returns text and way
    to the file as list: [text, way]
    Otherwise returns [None, way]
    """
    while True:
        file_name = input("Insert the way to your file >>>> ")
        if pathcheck(file_name):
            with open(file_name, 'r') as work_file:
                text = work_file.read()
            if notampty(text):
                return text, file_name
            else:
                return None, file_name
        else:
            print("Try again")


#  Task 1

text, _ = text_check()
if text:
    time_date_list = findall(r'\d{2}-\d{2}-\d{4}', text)
    print(time_date_list)


#  Task 2

while True:
    user_year = input("Type year in format YYYY (1999) >>>> ")

    if user_year.isdigit():
        if isleap(user_year):
            print('leap')
        else:
            print('not leap')
        # user_year = int(user_year)
        # march = date(user_year, 3, 1)
        # february = date(user_year, 2, 1)
        # delta = march - february
        # if delta.days == 29:
        #     print("It is a leap year")
        # else:
        #     print("It is not a leap year")
        # break
    else:
        print("Entered is not a numbe. Try again")

#  Task 3

text, _ = text_check()

if text:
    numbers = findall(r'-?\d+\.\d+|-?\d+', text)
    sum_of_numbers = fsum(float(x) for x in numbers)
    print(sum_of_numbers)


#  Task 4

def geom_progression(start, dominator, length):
    list_of_progression = [0] * length
    number = start
    for x in range(length):
        list_of_progression[x] = number
        number = number * dominator
    return list_of_progression


while True:
    start = input("Type the first number of progression >>>> ")
    dominator = input("Type the dominator of progression >>>> ")
    length = input("Type the lenght of progression >>>> ")

    if ifnumber(start) and ifnumber(dominator) and length.isdigit():
        start, dominator, length = (floatint(start), floatint(dominator),
                                    int(length))
        answer = geom_progression(start, dominator, length)
        print(f" Your progression: {answer}")
        break
    else:
        print("Not a number. Try again")


#  Task 5
#  Хотел написать один шаблон (r'-\d+\.?\d*'), но в тком случае удаляется
#  и точка самого предложения, если число стоит в конце предложения.

text, filename = text_check()
if text:
    non_negative_list = sub(r'-\d+\.\d+|-\d+', '', text)
    with open(filename, 'w') as rewrite_file:
        rewrite_file.write(non_negative_list)


#  Task 6

text, _ = text_check()
if text:
    list_of_names = findall(r'[А-Я][а-я]+-?[А-Я]*[а-я]*-?[А-Я]*[а-я]*', text)
    print(list_of_names)
