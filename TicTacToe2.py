import curses
import random


class TicTacToe:

    robot_vs_human = """
                                       ________
                                    / '   __    \ _
                                   (    \.~-~/.  \_ '
                   __ _______  ___/     )   (_,\_(\ _)
                        ___    __          ((_,_____ )
                         ____          .~--((_,_____)
                        ______              ((_,__)
                          ___   _  _\.___/_____ .'


                                vs

                           __   ___
                       .'__  ///___  '
                      .     |||    / .  '
                     (    ._|||__/    \  , _   ____ ________________
                    (    ( /     \   _ )/__ ///___  __________
                    (____'.       (/ __    |||       ____
                     /___ /.      | ('_)   |||    ______
                    (    ( (_   _ |\ __ ___|||__   __
                    (    '._ ///_/     )  '
                     (      |||  \    / .'
                       . ___|||___\_ '
    """
    human = 0
    robot = 0
    human_tool = ").("
    computer_tool = "(_)"
    win_positions = (
                    (0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
                    (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)
                    )
    end = [1]
    menu = ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "]
    moves = [
            [4, 2, 6, 0, 8], [7, 2, 6, 0, 8],
            [4, 2, 6, 0, 8], [2, 6, 0, 8, 7, 5, 3, 1]
                ]


    def player_move(self, position):
        self.menu.insert(position, self.human_tool)
        self.menu.pop(position + 1)


    def computer_move(self):
        if len(self.moves) == 0:
            return curses.wrapper(self.standoff)
        winner = self.winner_test()
        computer_check = self.check_for_winning(self.computer_tool)
        human_check = self.check_for_winning(self.human_tool)
        if winner:
            return curses.wrapper(self.congratulations, winner)
        elif computer_check is not None:
            self.menu.insert(computer_check, self.computer_tool)
            self.menu.pop(computer_check + 1)
            return curses.wrapper(self.congratulations, self.computer_tool)
        elif human_check is not None:
            self.menu.insert(human_check, self.computer_tool)
            self.menu.pop(human_check + 1)
            self.moves.pop(0)
        else:
            return self.robot_move()


    def winner_test(self):
        for position in self.win_positions:
            if self.menu[position[0]] == self.menu[position[1]] == self.menu[position[2]] and \
                    self.menu[position[0]] != "  ":
                return self.menu[position[0]]


    def check_for_winning(self, tool):
        for position in self.win_positions:
            if self.menu[position[0]] == self.menu[position[1]] == tool and \
                    self.menu[position[2]] == "  ":
                return position[2]
            elif self.menu[position[0]] == self.menu[position[2]] == tool and \
                    self.menu[position[1]] == "  ":
                return position[1]
            elif self.menu[position[1]] == self.menu[position[2]] == tool and \
                    self.menu[position[0]] == "  ":
                return position[0]


    def robot_move(self):
        if len(self.moves) == 4 and self.menu[4] == self.human_tool:
            self.moves[1].pop(0)
        if len(self.moves) == 4 and self.moves[0][0] == 2 and (
                self.menu[1] == self.human_tool or self.menu[5] == self.human_tool):
            self.menu.insert(6, self.computer_tool)
            self.menu.pop(7)
            self.moves.pop(0)
        elif len(self.moves) == 3 and self.menu[4] == self.computer_tool and (
                self.menu[3] == self.human_tool or self.menu[7] == self.human_tool) and self.menu[6] == "  ":
            self.menu.insert(6, self.computer_tool)
            self.menu.pop(7)
            self.moves.pop(0)
        else:
            for numb in self.moves[0]:
                if self.menu[numb] != self.human_tool and self.menu[numb] != self.computer_tool:
                    self.menu.insert(numb, self.computer_tool)
                    self.menu.pop(numb + 1)
                    self.moves.pop(0)
                    break


    def robot_first(self):
        self.menu.insert(8, self.computer_tool)
        self.menu.pop(9)
        self.moves[0].insert(0, 2)
        self.moves[0].pop(1)
        self.moves[1].pop(0)
        self.moves[1].insert(0, 4)



    def rock_scissors_paper(self, stdscr):
        while True:
            stdscr.clear()
            human = self.rock_scissors_paper_menu()
            robot = random.choice(('rock', 'scissors', 'paper'))
            curses.curs_set(False)
            height, width = stdscr.getmaxyx()
            if human != robot:
                result = human + robot
                stdscr.addstr(
                                height // 2, width // 2 - 7,
                                f"Your {human} against my {robot}"
                                )
                stdscr.getch()
                stdscr.refresh()
                stdscr.clear()
                if result in ('rockscissors', 'scissorspaper', 'paperrock'):
                    stdscr.addstr(height // 2, width // 2 - 7, "You won! You start first!")
                    stdscr.getch()
                    stdscr.refresh()
                    self.moves[2].pop(0)
                else:
                    stdscr.addstr(
                                    height // 2, width // 2 - 7,
                                    "Sorry, you lose( I start first!"
                                    )
                    stdscr.getch()
                    stdscr.refresh()
                    self.robot_first()
                stdscr.refresh()
                return curses.wrapper(self.main_2)
            else:
                stdscr.addstr(
                                height // 2, width // 2 - 7,
                                f"Your {human} against my {robot} is equal)"
                                )
                stdscr.getch()
            stdscr.refresh()


    def main_1(self, stdscr, menu):
        curses.curs_set(False)
        curses.init_pair(True, curses.COLOR_BLACK, curses.COLOR_WHITE)
        row_index = 0
        self.print_menu(stdscr, row_index, menu)

        while True:
            key = stdscr.getch()
            stdscr.clear()
            if key == curses.KEY_UP and row_index > 0:
                row_index -= 1
            elif key == curses.KEY_DOWN and row_index < len(menu) - 1:
                row_index += 1
            elif key == curses.KEY_ENTER or key in [10, 13]:
                stdscr.refresh()
                return row_index

            self.print_menu(stdscr, row_index, menu)
            stdscr.refresh()


    def print_bar(self, stdscr, position):
        stdscr.clear()
        height, width = stdscr.getmaxyx()
        counter = f"human = {self.human}   robot = {self.robot}"
        stdscr.addstr(2, width // 2 - 14, counter)
        y_correction = -5
        menu_position = 0

        for first_index in (0, 1, 2):
            x_correction = -15
            for second_index in (0, 1, 2):
                x = width // 2 - len(self.menu[menu_position]) // 2 + \
                    second_index + x_correction
                y = height // 2 - 1 + first_index + y_correction
                if position == menu_position:
                    stdscr.attron(curses.color_pair(True))
                    stdscr.addstr(y, x, self.menu[menu_position])
                    stdscr.attroff(curses.color_pair(True))
                    if second_index < 2:
                        stdscr.addstr(y - 2, x + 6,    "|")
                        stdscr.addstr(y - 1, x + 6,    "|")
                        stdscr.addstr(y, x + 6,        "|")
                        stdscr.addstr(y + 1, x + 6,    "|")
                        stdscr.addstr(y + 2, x + 6,    "|")
                    if first_index < 2:
                        stdscr.addstr(y + 3, x - 4,    "___________")
                else:
                    stdscr.addstr(y, x, self.menu[menu_position])
                    if second_index < 2:
                        stdscr.addstr(y - 2, x + 6,    "|")
                        stdscr.addstr(y - 1, x + 6,    "|")
                        stdscr.addstr(y, x + 6,        "|")
                        stdscr.addstr(y + 1, x + 6,    "|")
                        stdscr.addstr(y + 2, x + 6,    "|")
                    if first_index < 2:
                        stdscr.addstr(y + 3, x - 4,    "___________")
                x_correction += 10
                menu_position += 1
            y_correction += 5
        stdscr.refresh()


    def main_2(self, stdscr):
        curses.curs_set(False)
        curses.init_pair(True, curses.COLOR_BLACK, curses.COLOR_WHITE)
        position = 0
        self.print_bar(stdscr, position)

        while len(self.end) > 0:
            key = stdscr.getch()
            stdscr.clear()
            height, w = stdscr.getmaxyx()
            if key == curses.KEY_UP and position in (3, 4, 5, 6, 7, 8):
                position -= 3
            elif key == curses.KEY_DOWN and position in (0, 1, 2, 3, 4, 5):
                position += 3
            elif key == curses.KEY_LEFT and position in (1, 2, 4, 5, 7, 8):
                position -= 1
            elif key == curses.KEY_RIGHT and position in (0, 1, 3, 4, 6, 7):
                position += 1
            elif key == curses.KEY_ENTER or key in [10, 13]:
                self.player_move(position)
                self.computer_move()

            self.print_bar(stdscr, position)
            stdscr.refresh()


    def first_menu(self):
        menu = ["You first", "Robot first", "Rock Scissors Paper", "Exit"]
        choise = curses.wrapper(self.main_1, menu)
        if menu[choise] == "You first":
            return curses.wrapper(self.main_2)
        elif menu[choise] == "Robot first":
            self.robot_first()
            return curses.wrapper(self.main_2)
        elif menu[choise] == "Rock Scissors Paper":
            return curses.wrapper(self.rock_scissors_paper)
        else:
            return exit()


    def rock_scissors_paper_menu(self):
        menu = ["rock", "scissors", "paper"]
        choise = curses.wrapper(self.main_1, menu)
        return menu[choise]


    def print_menu(self, stdscr, row_index, menu):
        stdscr.clear()
        height, width = stdscr.getmaxyx()
        stdscr.addstr(height // 2 - 13, 0, self.robot_vs_human)
        for index, word in enumerate(menu):
            x = width // 2 - len(word) // 2
            y = height // 2 - len(menu) // 2 + index
            if index == row_index:
                stdscr.attron(curses.color_pair(True))
                stdscr.addstr(y, x, word)
                stdscr.attroff(curses.color_pair(True))
            else:
                stdscr.addstr(y, x, word)
        stdscr.refresh()


    def standoff(self, stdscr):
        stdscr.clear()
        self.end.pop()
        height, width = stdscr.getmaxyx()
        stdscr.addstr(height // 2, width // 2 - 10, "Standoff!)")
        stdscr.refresh()
        stdscr.getch()


    def congratulations(self, stdscr, tool):
        stdscr.clear()
        self.end.pop()
        height, width = stdscr.getmaxyx()
        if tool == self.human_tool:
            text = "You Win!"
            self.human += 1
        else:
            text = "You lose("
            self.robot += 1
        stdscr.addstr(height // 2, width // 2 - 10, text)
        stdscr.refresh()
        stdscr.getch()

    def begin(self):
        while True:
            self.end = [1]
            self.menu = ["  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "]
            self.moves = [
                    [4, 2, 6, 0, 8], [7, 2, 6, 0, 8],
                    [4, 2, 6, 0, 8], [2, 6, 0, 8, 7, 5, 3, 1]
                        ]
            self.first_menu()

# game = TicTacToe()
# game.begin()
