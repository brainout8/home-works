import sys
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import (
                                QPushButton, QApplication, QLabel, QWidget,
                                QGraphicsDropShadowEffect
                                )
from re import search, split, sub, findall
from functools import partial


class Example(QWidget):

    example = []
    picture1 = "one.png"
    parenth_control = []
    error = False
    answer = ""
    buttons_name = [
                    '(-', '**', '.', 'Remove', '1', '2', '3', '()',
                    '4', '5', '6', '/', '7', '8', '9', '*',
                    '+', '0', '-', '='
                    ]

    buttons_picture = [
                'negative1.png', 'power1.png', 'dot1.png', 'remove1.png',
                'one1.png', 'two2.png', 'three1.png', 'parenth1.png',
                'four1.png', 'five1.png', 'six1.png', 'division1.png',
                'seven1.png', 'eight1.png', 'nine1.png', 'multi1.png',
                'plus1.png', 'zero1.png', 'minus1.png', 'result2.png',
                ]

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):

        self.setFixedSize(330, 460)
        self.setWindowTitle('Your Calculator')
        self.setWindowIcon(QIcon('2x2_2.png'))

        label = QLabel(self)
        pixmap = QPixmap('calc5.png')
        label.setPixmap(pixmap)

        label1 = QLabel(self)
        pixmap1 = QPixmap('stripe.png')
        label1.setPixmap(pixmap1)
        label1.resize(250, 30)
        label1.move(43, 70)
        shadow = QGraphicsDropShadowEffect(self)
        shadow.setBlurRadius(10)
        shadow.setXOffset(4)
        shadow.setYOffset(4)
        label1.setGraphicsEffect(shadow)

        self.lbl = QLabel(self)
        self.lbl.move(45, 70)

        x_coord = 35
        y_coord = 150

        for button, butt_name in zip(self.buttons_picture, self.buttons_name):

            btn1 = QPushButton(self)
            btn1.setIcon(QIcon(button))
            btn1.setIconSize(QtCore.QSize(50, 50))
            btn1.resize(45, 45)
            btn1.move(x_coord, y_coord)
            btn1.clicked.connect(partial(self.add_choise, butt_name))
            btn1.clicked.connect(self.changed)
            self.shadow = QGraphicsDropShadowEffect(self)
            self.shadow.setBlurRadius(10)
            self.shadow.setXOffset(4)
            self.shadow.setYOffset(4)
            btn1.setGraphicsEffect(self.shadow)
            if x_coord == 245:
                x_coord = 35
                y_coord += 60
            else:
                x_coord += 70

        self.show()

    def changed(self):
        if self.error is True:
            text = "Error"
            self.example.clear()
            self.answer = ""
        else:
            if self.answer == "":
                text = ''.join(self.example)
            else:
                self.example = list(str(self.answer))
                text = ''.join(self.example)
                self.answer = ""
        self.lbl.setText(text)
        self.lbl.setStyleSheet('font: 23px;')
        self.lbl.adjustSize()
        self.error = False

    def add_choise(self, butt_name):
        if butt_name in "0123456789":
            return self.add_number(butt_name)
        elif butt_name in "*/+-" or butt_name == "**":
            return self.add_classic(butt_name)
        elif butt_name == "()":
            return self.add_parenth()
        elif butt_name == "(-":
            return self.add_negative()
        elif butt_name == ".":
            return self.add_dot()
        elif butt_name == 'Remove':
            return self.remove_func()
        elif butt_name == '=':
            return self.equal_func()

    def add_number(self, butt_name):
        if (len(self.example) == 0 or self.example[-1] != ")") and \
                len(self.example) < 19:
            self.example.append(butt_name)

    def add_classic(self, butt_name):
        if len(self.example) < 19:
            if len(self.example) != 0 and self.example[-1] in "0123456789)":
                self.example.append(butt_name)

    def add_parenth(self):
        if len(self.example) < 19:
            if len(self.example) == 0:
                self.example.append('(')
                self.parenth_control.append('(')
            elif self.example[-1] not in "0123456789" and \
                    self.example[-1] not in (".", "**", ")"):
                self.example.append('(')
                self.parenth_control.append('(')
            elif len(self.parenth_control) != 0:
                self.example.append(')')
                self.parenth_control.pop()

    def add_dot(self):
        if len(self.example) < 19:
            if len(self.example) != 0 and self.example[-1] in "0123456789":
                self.example.append('.')

    def add_negative(self):
        if len(self.example) < 18:
            if len(self.example) == 0 or self.example[-1] in "+-/*":
                self.example.append('(')
                self.example.append('-')
                self.parenth_control.append('(')

    def remove_func(self):
        if len(self.example) != 0:
            if self.example[-1] == "(":
                self.parenth_control.pop()
            elif self.example[-1] == ")":
                self.parenth_control.append('(')
            self.example.pop()

    def equal_func(self):
        test_string = ''.join(self.example)
        if len(self.example) == 0:
            pass
        elif len(self.parenth_control) == 0 and \
                self.example[-1] in "0123456789)":
            return self.getting_answer(test_string)
        else:
            self.error = True

    def getting_answer(self, test_string):
        try:
            self.answer = parentheses_search(test_string)
            self.answer = math_operation_search(self.answer)
            self.answer = self.float_test(self.answer)
            return self.changed()
        except ZeroDivisionError:
            self.error = True

    def float_test(self, answer):
        if answer[-1] == "0" and answer[-2] == ".":
            return answer[:-2]
        else:
            return answer


def math_decorator(func):
    def inner(power, example):
        start = example.find(power.group())
        end = start + len(power.group())
        result = func(power, example)
        example = example[:start] + str(result) + example[end:]
        return example
    return inner


@math_decorator
def power_func(power, example):
    new = split(r"\*{2}", power.group())
    answer = float(new[0]) ** float(new[1])
    return answer


@math_decorator
def multiply_func(multiply, example):
    new = split(r"\*{1}", multiply.group())
    answer = float(new[0]) * float(new[1])
    return answer


@math_decorator
def division_func(division, example):
    new = split(r"/{1}", division.group())
    answer = float(new[0]) / float(new[1])
    return answer


@math_decorator
def add_func(add, example):
    new = split(r"\+{1}", add.group())
    answer = float(new[0]) + float(new[1])
    return answer


@math_decorator
def subtract_func(subtract, example):
    subtract = subtract.group()
    if subtract.count("-") == 1:
        new = split(r"-{1}", subtract)
        answer = float(new[0]) - float(new[1])
    else:
        subtract = subtract[1:]
        new = split(r"-{1}", subtract)
        answer = -float(new[0]) - float(new[1])
    return answer


def math_operation_ans(example):
    example = sub(r"--", "+", example)
    power = search(r"-?\d+\.?\d*\*{2}-?\d+\.?\d*", example)
    if power:
        example = power_func(power, example)
        return example
    multiply = search(r"-?\d+\.?\d*\*{1}-?\d+\.?\d*", example)
    if multiply:
        example = multiply_func(multiply, example)
        return example
    division = search(r"-?\d+\.?\d*/{1}-?\d+\.?\d*", example)
    if division:
        example = division_func(division, example)
        return example
    add = search(r"-?\d+\.?\d*\+{1}-?\d+\.?\d*", example)
    if add:
        example = add_func(add, example)
        return example
    subtract = search(r"-?\d+\.?\d*-{1}\d+\.?\d*", example)
    if subtract:
        example = subtract_func(subtract, example)
        return example


def math_operation_search(example):
    test = search(r"-?\d+\.?\d*", example)
    test = example.replace(test.group(), "")
    if len(test) == 0:
        return str(round(float(example), 4))
    else:
        while len(test) > 0:
            example = math_operation_ans(example)
            test = search(r"-?\d+\.?\d*", example)
            test = example.replace(test.group(), "")
    return str(round(float(example), 4))


def parentheses_ans(example):
    right = example.find("(")
    left = parentheses_seq(example[right + 1:]) + right + 1
    new_example = example[right + 1:left]
    ans = parentheses_search(new_example)
    ans = math_operation_search(ans)
    example = example[:right] + ans + example[left + 1:]
    return example


def parentheses_search(example):
    right = example.find("(")
    if right == -1:
        return example
    else:
        while right != -1:
            example = parentheses_ans(example)
            right = example.find("(")
    return example


def parentheses_seq(example):
    left = 0
    for index, element in enumerate(example):
        if element == "(":
            left -= 1
        elif element == ")" and left == 0:
            return index
        elif element == ")":
            left += 1


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
