from time import time
from my_methods import ifnumber

#  Task 1

def decorator(arg):
    def time_counter(function):
        def wrapper(*args, **kwargs):
            if arg == 'run':
                start_time = time()
                result = function(*args, **kwargs)
                print('Function runtime = {:f}'.format(time() - start_time))
                return result
            else:
                return arg
        return wrapper
    return time_counter


@decorator('run')
def summ(x, y, z):
    return (x + y + z)


print("Summ = ", summ(7, 3, -23.954))


#  Task 2

while True:
    start_number = input("Please, type a first integer number here >>>> ")
    end_number = input("Please, type a second integer number here >>>> ")

    if ifnumber(start_number) and ifnumber(end_number):
        start_number, end_number = int(start_number), int(end_number)
        start = max(start_number, end_number)
        end = min(start_number, end_number)
        user_list = [x for x in range(start, end - 1, -1) if x % 3 == 0]
        print(f"Your sequence = {user_list}")
        break
    else:
        print("Wrong input. Try one more time.")



#  Task 3  Декоратор написал в первом задании, чтобы не повторяться.

@decorator('do not run')
def subtract(x, y, z):
    return (x - y - z)


print("Subtract = ", subtract(7, 3, -23.954))


#  Task 4

while True:
    user_numb = input("Please, type a natural number here >>>> ")

    if user_numb.isdigit() and int(user_numb) > 0:
        triangle_list = [
            int((x * x + x) / 2) for x in range(1, int(user_numb) + 1)
            ]
        print(f"Your triangle sequence = {triangle_list}")
        break
    else:
        print("Wrong input. Try one more time.")
