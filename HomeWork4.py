while True:
    user_number = input('Please, type your integer number here >>>> ')

    if user_number.isdigit():
        alphabet = "abcdefghijklmnopqrstuvwxyz" * 2

        with open('for_encoding.txt', 'r+') as my_file:
            text = my_file.read()
            encode_text = ''
            my_file.seek(0)
            for element in text:
                if element.islower():
                    element = alphabet[
                        alphabet.find(element) + int(user_number) % 26
                        ]
                elif element.isupper():
                    element = alphabet[
                        alphabet.find(element.lower()) + int(user_number) % 26
                        ].upper()
                elif element.isdigit():
                    element = (int(element) + int(user_number) % 10) % 10
                encode_text += str(element)
            my_file.write(encode_text)
        break
    else:
        print("Entered is not a number or integer. Please, try again")
