import asyncio
from time import time
from functools import reduce
from os import getcwd, mkdir, path
from random import uniform, randint


async def creation_files(number):
    with open(f"{path_to_files}/in_{number}.dat", 'w') as create_file:
        operation = str(randint(1, 4)) + "\n"
        numbers = []
        for element in range(10):
            element = round(uniform(0, 50), 5)
            numbers.append(str(element))
            numbers.append(" ")
        string_of_numbers = "".join(numbers)
        list_of_info = [operation, string_of_numbers]
        create_file.writelines(list_of_info)


async def wrighting_answers(number):
    with open(f"{path_to_files}/in_{number}.dat", 'r') as reed_file:
        info_file = reed_file.readlines()
        list_of_numbers = info_file[1].split(" ")
        list_of_numbers = [float(x) for x in list_of_numbers[:-1]]
        operation = int(info_file[0][0])
        if operation == 1:
            answer = sum(list_of_numbers)
        elif operation == 2:
            answer = list_of_numbers[0] - sum(list_of_numbers[1:])
        elif operation == 3:
            list_of_numbers.insert(0, 0)
            answer = reduce(lambda x, y: x + y**2, list_of_numbers)
        elif operation == 4:
            answer = reduce(lambda x, y: x * y, list_of_numbers)
    with open(f"out{name_number}.dat", 'a') as out_file:
        for_write = f"in_{number} {name_operation[operation]}: {answer}\n"
        out_file.write(for_write)


async def main_1():
    tasks = []
    for number in range(1, 151):
        tasks.append(asyncio.ensure_future(creation_files(number)))
    await asyncio.gather(*tasks)


async def main_2():
    tasks = []
    for number in range(1, 151):
        tasks.append(asyncio.ensure_future(wrighting_answers(number)))
    await asyncio.gather(*tasks)


if __name__ == "__main__":
    name_operation = {
                        1: "adding", 2: "subtraction",
                        3: "sum_of_squares", 4: "multiply"
                        }
    while True:
        path_to_files = input("Type your path to files here, or press Enter\n"
                    "to make files automatically >>>> ")
        name_number = str(int(time()))
        if path_to_files == "":
            path_to_files = getcwd()
            folder_name = f"/folder{name_number}"
            mkdir(path_to_files + folder_name)
            path_to_files += folder_name
            break
        elif path.exists(path_to_files):
            break
        else:
            print("Wrong path. Try again, please.")

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main_1())
    loop.run_until_complete(main_2())
