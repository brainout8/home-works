#  Функция проверки пути файла


def file_path_check(file_path):
    try:
        with open(file_path):
            return True
    except FileNotFoundError:
        print('Wrong way to file. File not Found')


#  Task 1

def int_to_roman(int_numb):
    """
    Function for translation
    arabian numbers to roman.
    Limit = MMMCMXCIX(3990)
    """
    integer = {1: "I", 5: "V", 10: "X", 50: "L", 100: "C", 500: "D", 1000: "M"}
    roster = 10**(len(str(int_numb)) - 1)
    list_of_ranges = []

    for number in str(int_numb):
        number = int(number)
        if number in [4, 9]:
            list_of_ranges.append(integer[int(roster)])
            list_of_ranges.append(integer[int((number + 1) * roster)])
        elif number in [1, 2, 3, 6, 7, 8]:
            if number >= 6:
                list_of_ranges.append(integer[int(5 * roster)])
                number -= 5
            for equal in range(number):
                list_of_ranges.append(integer[int(roster)])
        elif number == 5:
            list_of_ranges.append(integer[number * roster])
        roster /= 10
    return ''.join(list_of_ranges)


def roman_to_int(roman_string):
    """
    Function for translation
    roman numbers to arabian.
    Limit = MMMCMXCIX(3990)
    """

    romans = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
    num_str = [0]
    for number in roman_string:
        number = int(romans[i])
        if number > num_str[-1]:
            number -= num_str[-1]
            num_str.remove(num_str[-1])
        elif number == num_str[-1]:
            number += num_str[-1]
            num_str.remove(num_str[-1])
        num_str.append(i)
    return sum(num_str)


while True:
    arab_number = input("Type arabic number here from 1 to 3999 >>>> ")

    if arab_number.isdigit() and 0 < int(arab_number) < 4000:
        int_roman = int_to_roman(int(arab_number))
        print(int_roman)
        break
    else:
        print("Something went wrong. Try again")

while True:
    roma_namber = input("Type roman number(like: MMMCMXCIX) here >>>> ")

    try:
        test = roma_namber.replace(' ', '')
        if len(test) > 0:
            roman_int = roman_to_int(roma_namber)
            print(roman_int)
            break
        else:
            print("Number was not entered. Try again")
    except KeyError:
        print("Make sure that number you entered is right and try again")



#  Task 2
#  На случай отрицательных, вещественных и чисел через запятую ввожу replace
def no_numbers(file_name, text):
    with open(file_name, 'w') as redo_file:
        for number in range(48, 58):
            text = text.replace('-' + chr(number), '  ') \
                .replace(chr(number), ' ')
            text = text.replace('  ,', '').replace('  ', '') \
                .replace(' .', '')
        redo_file.write(text)


while True:
    file_name = input("Insert the way to your file >>>> ")

    if file_path_check(file_name):
        with open(file_name, 'r') as work_file:
            text = work_file.read()
        test = text.replace(' ', '').replace('\n', '')
        if len(test) == 0:
            print("The file is empty")
        else:
            no_numbers(file_name, text)
        break


#  Task 3, Task 4
#  Задание 4 работает вместе с заданием 3.
#  Дешифровка реализуется той же функцией что и шифровка, с передачей шага
#  в отрицательном значении. Поэтому объединил в одном задании оба.

def caesar_encrypt(text, remove):
    encrypt_text = ''
    for element in text:
        if element.islower():
            element = alphabet[
                alphabet.find(element) + remove % 26
                ]
        elif element.isupper():
            element = alphabet[
                alphabet.find(element.lower()) + remove % 26
                ].upper()
        elif element.isdigit():
            element = (int(element) + remove % 10) % 10
        encrypt_text += str(element)
    return encrypt_text


alphabet = "abcdefghijklmnopqrstuvwxyz" * 2

while True:
    text = input("Insert the text for Caesar encription >>>> ")
    number = input("Type the number of displacement >>>> ")

    test = text.replace(' ', '')
    if len(test) != 0 and number.isdigit():
        break
    else:
        print("Ampty string or not a number. Try again")

while True:
    print("What you want to do?")
    choise = input("For Encrypt type E, for Decrypt type D >>>> ")
    if choise in "ED":
        number = int(number)
        if choise == 'E':
            encrypt_text = caesar_encrypt(text, number)
            print(encrypt_text)
        else:
            decrypt_text = caesar_encrypt(text, -number)
            print(decrypt_text)
        break
    else:
        print("For Encrypt type E, for Decrypt type D. Try again")


#  Task 5
#  Принимает функцию из задания 3.
def write_caesar(file_for_coding, number):
    with open(file_for_coding, 'r') as normal_file:
        text = normal_file.read()
        encrypt_text = caesar_encrypt(text, number)
        with open('caesar_coding.txt', 'w') as caesar_file:
            caesar_file.write(encrypt_text)


while True:
    file_for_coding = input("Insert the way to your file >>>> ")
    number = input("Type the number of displacement >>>> ")

    if not number.isdigit():
        print("Not a number. Try again")
    elif file_path_check(file_for_coding):
        with open(file_for_coding, 'r') as work_file:
            text = work_file.read()

        test = text.replace(' ', '').replace('\n', '')
        if len(test) == 0:
            print("The file is empty")
        else:
            number = int(number)
            write_caesar(file_for_coding, number)
        break


#  Task 5.1 Работает с Task 3
#  Изначально неверно прочел задание 5 и делал функцию с текстом как
#  принимаемым аргументом. Поэтому пробовал написать функцию, которая еще и
#  разделяет текст на строки для файла. Вставляю аргументы сам,
#  просто для проверки.

def write_caesar(text, number):
    with open('test_coding.txt', 'w') as caesar_file:
        encrypt_text = caesar_encrypt(text, number) + ' '
        start = 0
        end = 70    # Приблизительная длина строки
        string_list = []
        for number in range(len(encrypt_text) // 70 + 1):
            position = encrypt_text.rfind(' ', start, end)
            string_list.append(encrypt_text[start:position + 1])
            start = position + 1
            end = position + 70
        caesar_file.write('\n'.join(string_list))


text = "Harry's mouth twisted bitterly. He was treated well, \
probably better than most genetic fathers treated their own children. \
Harry had been sent to the best primary schools - and when that didn't \
work out, he was provided with tutors from the endless pool of starving \
students. Always Harry had been encouraged to study whatever caught his \
attention, bought all the books that caught his fancy, sponsored in whatever \
maths or science competitions he entered. He was given anything reasonable \
that he wanted, except, maybe, the slightest shred of respect."
number = 3

choice = input("For encrypting input Y. For skip press Enter >>>> ")

if choice == 'Y':
    write_caesar(text, number)
