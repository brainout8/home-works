#  Task 1

user_name = input("Please, type your name here >>>> ")
user_age = input("Type your age here >>>> ")

print("Hello", user_name, "\nYour age is:", user_age)

#  Task 2

user_number = int(input("Type your number here >>>> "))

power_132 = user_number**132
division_remain = user_number % 3

print("Your number to the power 132 =", power_132)
print("Remain from division by three =", division_remain)

#  Task 3

first_number = int(input("Type first number here >>>> "))
second_number = int(input("Type second number here >>>> "))

print(
        "Sum of your numbers =", first_number + second_number,
        "\nDifference of your numbers =", first_number - second_number,
        "\nResult from multiplication =", first_number * second_number,
        "\nResult from division =", first_number / second_number,
        "\nInteger from division =", first_number // second_number,
        "\nRemainder from division =", first_number % second_number,
        "\nResult from exponentiation =", first_number**second_number
        )

#  Task 4

first_number = int(input("Type first number(a) here >>>> "))
second_number = int(input("Type second number(b) here >>>> "))
third_number = int(input("Type third number(c) here >>>> "))

#  Formula: 2a - 8b / (a - b + c)
result = 2 * first_number - 8 * second_number / \
        (first_number - second_number + third_number)

print("Result from using your numbers by formula: 2a-8b/(a-b+c) =", result)

#  Task 5

user_string = input("Type your string here >>>> ")
user_number = int(input("Type your number here >>>> "))

string_multiply = user_string * user_number

print("Result from multiplication of your string and number =", string_multiply)

#  Task 6

first_number = 125
second_number = 437

division_by_2_remainder = first_number % 2
division_by_3_remainder = first_number % 3
division_by_10_remainder = first_number % 10
division_by_22_remainder = first_number % 22

print(
        "For number 125:"
        "\nResult after division by 2 =", division_by_2_remainder,
        "\nResult after division by 3 =", division_by_3_remainder,
        "\nResult after division by 10 =", division_by_10_remainder,
        "\nResult after division by 22 =", division_by_22_remainder
        )

division_by_2_remainder = second_number % 2
division_by_3_remainder = second_number % 3
division_by_10_remainder = second_number % 10
division_by_22_remainder = second_number % 22

print(
        "For number 437:"
        "\nResult after division by 2 =", division_by_2_remainder,
        "\nResult after division by 3 =", division_by_3_remainder,
        "\nResult after division by 10 =", division_by_10_remainder,
        "\nResult after division by 22 =", division_by_22_remainder
        )

#  Task 7

first_number = int(input("Type first number here >>>> "))
second_number = int(input("Type second number here >>>> "))

integer_division = first_number // second_number

print("Integer from division of your numbers =", integer_division)

#  Task 8

first_string = input("Type first string here >>>> ")
second_string = input("Type second string here >>>> ")
third_string = input("Type third string here >>>> ")

first_string = first_string + " " + second_string + " " + third_string

print("Result from concatination of your strings =", first_string)

#  Task 9

first_number = 15
second_number = 43

temporary = first_number
first_number = second_number
second_number = temporary

print(
        "After numbers replacement: first number =", first_number,
        "second number =", second_number
        )
