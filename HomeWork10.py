from datetime import date, timedelta
from my_methods import ifnumber


#  Task 1

class Booking:

    today = date.today()
    year = "date:" + str(today.year)
    month = year + "-" + str(today.month)
    week_day = date.today().weekday()
    count_amount = 0
    count_common = 0
    week = []
    period = {'day': str(today), 'week': week, 'month': month, 'year': year}

    def week_days(self):
        n = self.week_day
        while n != -1:
            days_count = timedelta(days=n)
            self.week.append(str(self.today - days_count))
            n -= 1

    def adding(self, name, amount, price, choise):
        if choise == 'arrival':
            file_name = "arrival list.txt"
        elif choise == 'selling':
            file_name = "selling list.txt"
        with open(file_name, "a") as add_list:
            info = f"date:{self.today}^ name:{name}^ amount:{amount}^ " + \
                    f"price:{price}^ common:{float(amount) * float(price)}\n"
            add_list.write(info)
            print("Product added to list")

    def summ_of_(self, inputStr):
        list_item = inputStr.split("^ ")
        self.count_amount += float(list_item[2][7:])
        self.count_common += float(list_item[4][7:-1])

    def data_searching(self, text_file, period, product):
        data = self.period[period]
        with open(text_file, "r") as choise_list:
            choise_text = choise_list.readlines()
            if period == 'week':
                self.week_days()
                for day in self.week:
                    for line in choise_text:
                        if (day in line) and (product in line):
                            self.summ_of_(line)
            else:
                for line in choise_text:
                    # search = line.find(data)
                    # if search != -1:
                    if data in line:
                        self.summ_of_(line)
            amount = self.count_amount
            common = self.count_common
            self.count_amount = self.count_common = 0
            return amount, common

    def info(self, info_choise, period, product):
        if product is None:
            product = ''
            name_2 = name = ''
        else:
            name_2 = name = f" of {product}"
        if info_choise == 'proffit':
            sell_amount, sell_common = self.data_searching(
                                        "selling list.txt", period, product
                                        )
            arrival_amount, arrival_common = self.data_searching(
                                        "arrival list.txt", period, product
                                        )
            sale_buy_diff = arrival_amount - sell_amount
            proffit = sell_common - arrival_common
            if product != '':
                print(
                        f"Sale-Buy difference{name_2}"
                        f" for last {period} = {sale_buy_diff}"
                        )
            print(
                    f"Proffit from selling{name_2}"
                    f" for last {period} = {proffit}$"
                    )
        else:
            text_file = info_choise + " list.txt"
            amount, common = self.data_searching(text_file, period, product)
            if product != '':
                print(
                        f"Number{name} {info_choise}"
                        f" for last {period} = {amount}"
                        )
            print(
                    f"Total {info_choise} amount{name}"
                    f" for last {period} = {common}$"
                    )


class AddChoise(Booking):

    def add_input(self, choise):
        while True:
            print("For return enter 'return'")
            print("Enter the name of product")
            product_name = input("Name can't take a '^' symbol >>>> ")
            search = product_name.find('^')
            if search != -1:
                print("Name can't take a '^' symbol!")
            elif product_name == 'return':
                return self.add_menu()
            else:
                new = self.new_item(choise + " list.txt", product_name)
                if new:
                    product_amount = input(
                                    "Enter weight or amount of product >>>> "
                                    )
                    price = input("Enter the price per item or per kg >>>> ")
                    amount_test = ifnumber(product_amount)
                    price_test = ifnumber(price)
                    if product_name and amount_test and price_test:
                        if choise == 'arrival':
                            self.adding(product_name,
                                        product_amount, price, choise)
                        else:
                            self.adding(product_name,
                                        product_amount, price, choise)
                    else:
                        print("Wrong input. Try again")

    def add_menu(self):
        while True:
            print("What kind of information would you like to add?")
            print("For return enter 'return'")
            add_choise = input("'arrival' or 'selling' of goods >>>> ")
            if add_choise == 'return':
                break
            elif add_choise == 'arrival':
                self.add_input(choise='arrival')
            elif add_choise == 'selling':
                self.add_input(choise='selling')
            else:
                print("Wrong input. Try again")

    def new_item(self, text_file, product):
        with open(text_file, 'r') as test_file:
            text = test_file.readlines()
            for line in text:
                if line.find(product) != -1:
                    return True
            while True:
                print("This kind of product is not in the list")
                choise = input("Add this like new? 'Yes', 'No' >>>> ")
                if choise in ['Yes', 'No']:
                    if choise == 'Yes':
                        return True
                    else:
                        break
                else:
                    print("Wrong input. Try again")


class InfoChoise(Booking):

    info_choise = ''
    period_choise = ''
    prod_choise = None

    def product_choise(self):
        while True:
            print("For return enter 'return'")
            print("Input product name or press Enter for all products")
            prod_choise = input(">>>> ")
            if prod_choise == 'return':
                return self.info_input()
            elif prod_choise == '':
                self.prod_choise = None
                self.info(
                        self.info_choise,
                        self.period_choise, self.prod_choise
                        )
            else:
                self.prod_choise = prod_choise
                self.info(
                        self.info_choise,
                        self.period_choise, self.prod_choise
                        )

    def info_input(self):
        while True:
            print("For return enter 'return'")
            print(
                "If you want to get info,"
                " type: 'day', 'week', 'month' or 'year'")
            period_choise = input(">>>> ")
            if period_choise in ['day', 'week', 'month', 'year']:
                self.week = []
                self.period_choise = period_choise
                self.product_choise()
            elif period_choise == 'return':
                return self.get_info_menu()
            else:
                print("Wrong input. Try again")

    def get_info_menu(self):
        while True:
            print("What kind of information would you like to get?")
            print("For return enter 'return'")
            info_choise = input(
                            "'arrival', 'selling', 'proffit' of goods >>>> "
                            )
            if info_choise == 'return':
                return
            elif info_choise in ['arrival', 'selling', 'proffit']:
                self.info_choise = info_choise
                self.info_input()
            else:
                print("Wrong input. Try again")


while True:
    print("What do you want to do?")
    print("For adding a new information enter:'add'")
    print("For information enter:'info'")
    print("For exit application enter:'exit'")
    choise = input(">>>> ")
    if choise == "exit":
        exit()
    elif choise not in ['add', 'info']:
        print("Wrong input, try again")
    else:
        if choise == 'add':
            user_choise = AddChoise()
            user_choise.add_menu()
        else:
            user_choise = InfoChoise()
            user_choise.get_info_menu()
