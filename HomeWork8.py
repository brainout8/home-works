from datetime import datetime, date
from time import sleep
import random

#  Task 1


class Car:

    model = "Lada"
    color = "white"
    wheel_position = "left"
    headlights = "Off"
    engine = "Off"
    max_speed = 240

    def __init__(self, model="Lada", color="white", wheel_position="left"):
        self.model = model
        self.color = color
        self.wheel_position = wheel_position

    def start_engine(self):
        self.engine = "On"
        print(f"Your {self.model} is ready to go")
        self._headlights_control()

    def _headlights_control(self):
        real_time = datetime.now().time()
        hours = int(str(real_time)[:2])
        if hours >= 17 or hours <= 8:
            self.headlights = "On"
            print("It's to dark. Headlights were on")
        else:
            self.headlights = "Off"

    def moving(self, max_speed=240):
        self.max_speed = max_speed
        if self.engine == "Off":
            print("At first you should star the engine!")
        elif max_speed >= 300 and self.headlights == 'On':
            print(
                    "Fast car and night time are bad friends. "
                    "Be careful on the road!"
                    )
        else:
            print("Have a nice ride!")

    def stop(self):
        self.engine = "Off"
        self.headlights = "Off"

    def door_oppening(self, door):
        """Atribute 'door' accepts: "left",
        "right", "back left", "back right"""
        if self.engine == "On":
            print("Engine is on. At first stop the car")
        else:
            if door == self.wheel_position:
                print(f"{door.capitalize()} light is on")
                print("Don't forget your car keys!")
            elif door not in ("left", "right", "back left", "back right"):
                print("Wrong command! Check your 'door' value")
            else:
                print(f"{door.capitalize()} light is on")


class Airplain:

    pilot_name = "pilot"
    capacity = 50
    height_limit = 1000
    permission = True
    height = 0

    def __init__(self, pilot_name="pilot", capacity=50, height_limit=1000):
        self.pilot_name = pilot_name
        self.capacity = capacity
        self.height_limit = height_limit

    def __lifetime_check(self, year_of_issue):
        year_test = date.today().year - year_of_issue
        if year_test > 30:
            self.permission = False

    def __waiting(self, passengers, minimal_number):
        print("Waiting...")
        sleep(5)
        more_passengers = random.choice(
            range(minimal_number - passengers, self.capacity - passengers + 1)
            )
        self.passengers = passengers + more_passengers
        print(f"{more_passengers} passengers boarded")

    def __ammount(self, passengers):
        difference = self.capacity - passengers
        minimal_number = round(self.capacity * 20 / 100) + 1
        if difference < 0:
            print(
                    "To much people."
                    f"We need to take off the flight {difference} of them."
                    )
            self.passengers = self.capacity
        elif passengers < minimal_number:
            while True:
                waiting = input(
                                "Very few passengers. "
                                "Are you sure that all are on board?"
                                "\nYes/No >>>> "
                                )
                if waiting == 'Yes':
                    self.passengers = passengers
                    break
                elif waiting == 'No':
                    self.__waiting(passengers, minimal_number)
                    break
                else:
                    print("Wrong input. Try again.")
        else:
            self.passengers = passengers

    def fly(self, year_of_issue, passengers):
        self.__lifetime_check(year_of_issue)
        self.__ammount(passengers)
        if self.permission is False:
            print("This airplain is to old! Forbidden to fly!")
        else:
            print(f"Have a nice flight, {self.pilot_name}!")
            self.height += 1000

    def up_height(self, value):
        new_height = self.height + value
        if new_height > self.height_limit:
            print(
                    f"Limit of height = {self.height_limit}. "
                    "Plain can't take a new height."
                    )
        else:
            self.height = new_height
            print(f"New height = {self.height}")

    def landing(self):
        print("Landing")
        self.height = 0


class Ship:

    """ For ancor: True == "Down"; False == "Up" """
    name = "Your ship"
    color = "white"
    anchor = True
    max_speed = 300
    way = None

    def __init__(self, name="Your ship", color="white", max_speed=300):
        self.name = name
        self.color = color
        self.max_speed = max_speed

    def anchoring(self):
        self.anchor = not self.anchor

    def moving(self, way):
        if self.anchor is not False:
            print("You mast to raise the anchor first")
        elif way in ("forward", "backward"):
            self.way = way
            print(f"{self.name} is moving {self.way}")
        else:
            print("Wrong way! Ship not moving.")

    def signal(self, phrase):
        if phrase == "SOS" or phrase == "Mayday":
            print("Rescue boat is on its way")
        else:
            print("It's not something importent. If you need help, print SOS.")

    def stop(self):
        if self.way is not None:
            self.anchor = "Down"
            self.way = None
            print("Stop the engine")
        else:
            print("Your boat is not moving already.")


class Amphibian(Car, Ship):

    name = "Ampha"
    color = "white"
    wheel_position = "left"
    on_water = False
    max_speed = 260

    def __init__(self, name="Ampha", color="white", wheel_position="left"):
        self.name = name
        self.color = color
        self.wheel_position = wheel_position

    def start_engine(self, ground_or_water):
        self.engine = "On"
        if ground_or_water == 'water':
            self.on_water = True
        print(f"Your {self.name} is ready to go")
        self._headlights_control()

    def door_oppening(self, door):
        if self.on_water is False:
            print("Vehicle is on the water. Doors are blocked")
        elif self.engine == "On":
            print("Engine is on. At first stop the car")
        else:
            if door == self.wheel_position:
                print("Don't forget your car keys!")
            elif door not in ("left", "right", "back left", "back right"):
                print("Wrong command!")
            else:
                print(f"{door.capitalize()} light is on")

    def anchoring(self):
        print("Amphibian has no anchor.")


bmw = Car(model='BMW', color='white', wheel_position='right')
bmw.start_engine()
bmw.moving(max_speed=350)
bmw.stop()
bmw.door_oppening(door="right")


airplain = Airplain(pilot_name="Arnold", capacity=25)
airplain.fly(year_of_issue=2000, passengers=4)
airplain.up_height(value=500)
airplain.landing()


boat = Ship(name="Thunder in paradise", color="white", max_speed=300)
boat.anchoring()
boat.moving(way="forward")
boat.signal(phrase="SOS")
boat.stop()


hybrid = Amphibian(name="Thunder in paradise", color="white")
hybrid.start_engine(ground_or_water="ground")
hybrid.moving(max_speed=350)
hybrid.signal("Hi!")
