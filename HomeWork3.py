#  Task 1

my_text = """Every inch of wall space is covered by a bookcase?! \
Each bookcase has six shelves, going almost to the ceiling. \
Some bookshelves are stacked to the brim with hardback books: \
science, maths, history, and everything else. Other shelves have \
two layers of paperback science fiction, with the back layer of \
books propped up on old tissue boxes or lengths of wood, so that \
you can see the back layer of books above the books in front. \
And it still isn't enough. Books are overflowing onto the tables \
and the sofas and making little heaps under the windows."""

#  Так как в условии задачи отсутствует пользователь, то проверку на него
#  делать не нужно, но проверку на дефисы (единственный знак препинания,
#  окруженный пробелами) я написал. И split('. ') с пробелом после точки,
#  так как в случае многоточия, подсчет предложений будет выходить неверный.
text_words = my_text.count(' ') + 1 - my_text.count(' - ')
text_sentences = my_text.count('.') + my_text.count('!') + \
                    my_text.count('?') - my_text.count('...')*2 - \
                    my_text.count('?!')

print(
        f"Number of words in text = {text_words}"
        f"\nNumber of sentences in text = {text_sentences}"
        )

#  Task 2

while True:
    user_number = input('Please, type your integer number here >>>> ')

    #  Так как метод isdigit воспринимает только натуральные числа,
    #  делаю проверку на минус перед числом.
    if user_number.count('-') == 1:
        user_number = user_number.lstrip('-')

    if user_number.isdigit():
        if int(user_number) % 2 == 0:
            print("Your number is Even")
        else:
            print("Your number is Odd")
        break
    else:
        print("Entered is not a number or integer. Please, try again")

#  Task 3

while True:
    user_values = input("Type numbers here separeted by comma and space >>>> ")

    list_of_values = []

    #  Проверка первого и последнего элемента, чтобы в случае пропуска
    #  пользователем числа, split(, ) не создал элемент строки ''.
    # if user_values[0] != ',' and user_values[-1] not in ', ':
    user_values = user_values.strip(',').rstrip(' ')
    list_of_strings = user_values.split(', ')

    #  Выполняется проверка на верность расстановки точки и тире
    #  и исключениe их для проверки на isdigit():
    for value in (list_of_strings):
        if value.count('-') == 1:
            value = value.lstrip('-')
        if value.count('.') == 1:
            value = value.replace('.', '')
        list_of_values.append(value)

    string_of_values = ''.join(list_of_values)

    if string_of_values.isdigit():
        list_of_values = []
        list_of_strings = user_values.split(',')
        #  Так как возможны и вещественные числа, во избежании ошибки
        #  перевожу все во float:
        list_of_values = [float(x) for x in list_of_strings]
        #  Для красоты вывода списка, чтобы целые числа не выводились как
        #  float, вместо генератора списка выше, можно использовать цикл ниже:
        # for number in list_of_strings:
        #     if round(float(number)) != float(number):
        #         list_of_values.append(float(number))
        #     else:
        #         list_of_values.append(int(number))
        list_of_values.sort()

        if len(list_of_values) < 10:
            list_of_values.reverse()

        print(f"Sorted values = {list_of_values}")
        break
    else:
        print("Must be a numbers separeted by comma and space. Try again")

#  Task 4

while True:
    first_number = input("Please, type a first number >>>> ")
    second_number = input("Please, type a second number >>>> ")

    list_of_values = []

    for value in (first_number, second_number):
        if value.count('-') == 1:
            value = value.lstrip('-')
        if value.count('.') == 1:
            value = value.replace('.', '')
        list_of_values.append(value)

    string_of_values = ''.join(list_of_values)

    if string_of_values.isdigit():
        first, second = float(first_number), float(second_number)
        if first > second:
            print(f"The difference of your numbers = {first - second}")
        elif first < second:
            print(f"The summ of your numbers = {first + second}")
        else:
            print(
                    f"The first number to the power of the second = "
                    f"{first**second}"
                    )
        break
    else:
        print("Values must be a numbers. Try again")

#  Task 5

while True:
    a_number = input("Please, type a first(a) number >>>> ")
    b_number = input("Please, type a second(b) number >>>> ")
    c_number = input("Please, type a third(c) number >>>> ")

    list_of_values = []

    for value in (a_number, b_number, c_number):
        if value.count('-') == 1:
            value = value.lstrip('-')
        if value.count('.') == 1:
            value = value.replace('.', '')
        list_of_values.append(value)

    string_of_values = ''.join(list_of_values)

    if string_of_values.isdigit():
        a_number, b_number, c_number = \
            float(a_number), float(b_number), float(c_number)

        #  Using formula 2a - 8b / (a-b+c):
        denominator = (a_number - b_number + c_number)

        if denominator == 0:
            print("Division by zero! Check your information and try again")
        else:
            formula = 2 * a_number - 8 * b_number / denominator
            print(
                    f"Result from using numbers by formula: 2a - 8b / (a-b+c)"
                    f" = {formula}"
                    )
            break
    else:
        print("Values must be a numbers. Try again")

#  Task 6

my_list = [3, 97, 77, 5, 8, 9, 4, 45, 68, 1, 32, 90, 54, 65]
counter = 0

for number in my_list:
    if number % 2 == 0:
        counter += 1

print(f"The number of even in list = {counter}")

#  Task 7

my_list = [3, 97, 77, 5, 8, 1, 9, 97, 4, 45, 68, 1, 32, 90, 54, 65]
sorted_list = []

for number in my_list:
    counter = False
    for sort_number in sorted_list:
        if number < sort_number:
            sorted_list.insert((sorted_list.index(sort_number)), number)
            counter = True
            break
    if counter is False:
        sorted_list.append(number)

print(f"Sorted list: {sorted_list}")

#  Task 8

while True:
    length = input(
                    "Type your number(number must be > 0 and integer) "
                    "that matches the length of the Fibonacci sequence >>>> "
                    )

    #  Так как последовательность может состоять и из одного элемента, то:
    if length.isdigit() and int(length) == 1:
        print(f"Fibonacci sequence: [1]")
        break
    elif length.isdigit() and int(length) > 0:
        sequence = [1, 1]
        for number in range(int(length) - 2):
            number = sequence[-2] + sequence[-1]
            sequence.append(number)
        print(f"Fibonacci sequence: {sequence}")
        break
    else:
        print("For sequence value must be an integer and > 0. Try again")

#  Task 9

while True:
    user_number = input("Type your number(integer and > 0) here >>>> ")
    counter = False

    if user_number.isdigit() and int(user_number) > 0:
        for number in range(2, round(int(user_number)**0.5) + 1):
            if int(user_number) % number == 0:
                print("Your number is composite.")
                counter = True
                break
        if counter is False:
            print("Your number is prime.")
        break
    else:
        print("Value must be an integer and > 0. Try again")
